
# LIS 4369 Extensible Enterprise Solutions

## Jonathan Poteet

### Project 1 Requirements:

*5 things*

1. Code and run demo.py
2. Backwards engineer the screenshots
3. Test pip freeze
4. Install: pandas, pandas_datareader, and matplotlib
5. Create at least three functions that are called by the program:
    * a. main(): Calls at least two other functions
    * b. get_requirements(): displays the program requirements
    * c. data_analysis_1(): displays the following data

#### README.md file should include the following items:

* Screenshots of the running program
* Screenshot of the graph
* Screenshot of the code
 

#### Assignment Screenshots:

*Running in Visual Studio part 1*:

![Visual Studio Screenshot](/p1/images/results1.PNG)

*Running in Visual Studio part 2*:

![Visual Studio Screenshot](/p1/images/result2.PNG)

*Running in IDLE part 1*:

![IDLE Screenshot](/p1/images/idle1.PNG)

*Running in IDLE part 2*:

![IDLE Screenshot](/p1/images/idle2.PNG)

*Graph*:

![Graph Screenshot](/p1/images/graph.PNG)

*Code part 1*:

![Code Screenshot](/p1/images/code1.PNG)

*Code part 2*:

![Code Screenshot](/p1/images/code2.PNG)
