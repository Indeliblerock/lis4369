

def get_requirements():
    print("\n\nProgram Requirements: \n\n"
    + "1. Run demo.py\n"
    + "2. If errors, more than likely missing installations.\n"
    + "3. Test python package installer: pip freeze\n"
    + "4. Research how to do the following installations.\n"
    + "\ta. pandas\n"
    + "\tb. pandas-datareader\n"
    + "\tc. matplotlib\n"
    + "5. Create at least 3 functions that are called by the program.\n"
    + "\ta. main(): calls at least 2 other functions\n"
    + "\tb. get_requirements(): Displays the program requirements\n"
    + "\tc. data_analysis_1(): Displays the following data.\n")

def data_analysis_1():
    import pandas as pd
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as plt
    from matplotlib import style


    start = datetime.datetime(2010,1,1)
    end = datetime.datetime.now()

    df = pdr.DataReader("XOM","yahoo", start, end)
    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint Columns: ")
    print(df.columns)

    print("\n Print Data Frame: \n")
    print(df)

    print("\nPrint first five lines: \n")
    print(df.head(5))
    #1st print of lines
    print("\nPrint last five lines: \n")
    print(df.tail(5))
    #2nd print of lines
    print("\nPrint first 2 lines: \n")
    print(df.head(2))
    #3rd print of lines
    print("\nPrint last 2 lines: \n")
    print(df.tail(2))
    #4th print of lines

    #style.use('fivethirtyeight')   
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()