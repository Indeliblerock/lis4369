# LIS 4369 Extensible Enterprise Solutions 

## Jonathan Poteet

### Assignment 2 Requirements:

*Three Parts*

1. Backward Engineer (using Python) the screenshots
2. The program should be organized with two modules
    - a) functions module contains the following functions
        - get_requirements()
        - calculate_payroll()
        - print_pay()
     - b) main module imports the functions module, and calls the functions.
3. Test your program using IDLE, and Visual Studio Code. 

#### README.md file should include the following items:
* Assignment Requirements
* Screenshots


#### Assignment Screenshots:

*main Code*:

![main.py Code](/a2/images/maincode.PNG)

*functions Code*:

![functions.py Code](/a2/images/functionscode.PNG)

*Payroll No Overtime*:

![Payroll No Overtime](/a2/images/payrollNoOvertime.PNG)

*Payroll with Overtime*:

![Payroll with Overtime](/a2/images/payrollWithOvertime.PNG)