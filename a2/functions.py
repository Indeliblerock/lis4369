def get_requirements():
    print("\nPayroll Calculator\n")
    print("Program Requirements\n"
    + "1. Must use float data type for user input.\n"
    + "2. Overtime Rate: 1.5 times hourly rate (hours over 40).\n"
    + "3. Holiday Rate: 2.0 times hourly rate (all holiday hours)\n"
    + "4. Must format currency with dollar sign, and round to two decimal places\n"
    + "5. Create at least three functions that are called by the program:\n"
    + "\ta. main(): Calls at least two other functions.\n"
    + "\tb. get_requirements(): Displays the program requirements.\n"
    + "\tc. calculate_payroll(): Calculates an individual's one week paycheck.\n")

def calculate_payroll():
    print("\nInput: ")
    hoursWorked = float(input("Enter hours worked: "))
    holidayHours = float(input("Enter holiday hours: "))
    hourlyPayRate = float(input("Enter hourly pay rate: "))

    if (hoursWorked <= 40):
        baseHours = hoursWorked
    elif (hoursWorked > 40):
            baseHours = 40
    
    global basePay 
    global overtimePay 
    global holidayPay 
    global grossPay    

    overtimeHours = hoursWorked - baseHours
    basePay = (hourlyPayRate) * baseHours
    overtimePay = ((hourlyPayRate * 1.5)) * overtimeHours
    holidayPay = ((hourlyPayRate * 2)) * holidayHours
    grossPay = basePay + overtimePay + holidayPay 
     
   
def print_pay(): 
    print("\nOutput:")
    print("{0:17} {1:>3}{2:.2f}".format("Base:", "$", basePay) 
    + "\n" + "{0:17} {1:>3}{2:.2f}".format("Overtime:", "$", overtimePay)
    + "\n" + "{0:17} {1:>3}{2:.2f}".format("Holiday:", "$", holidayPay)
    + "\n" + "{0:17} {1:>3}{2:.2f}".format("Gross:", "$", grossPay))