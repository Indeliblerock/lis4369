
# LIS 4369 Extensible Enterprise Solutions

## Jonathan Poteet

### Assignment 5 Requirements:

*5 Items*

1. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
2. Code and run lis4369_a5.R
3. Be sure to include at least two plots
4. Be sure to test your program using RStudio
5. Use R to perform data analysis on Titanic Data 

#### README.md file should include the following items:

* Assignment requirements
* Screenshots for the R Tutorials
* Screenshots for Assignment 5


#### Assignment Screenshots:

*Screenshots For the R Tutorials*

![Full Screen R Tutorial screenshot](images/tutorial.PNG)
![Full Screen R Tutorial screenshot](images/tutorial_console.PNG)
![Plot](images/tutorial1.PNG)
![Plot](images/tutorial2.PNG)
![Plot](images/tutorial3.PNG)
![Plot](images/tutorial4.PNG)
![Plot](images/tutorial5.PNG)


*Screenshots for the R Assignment 5*:

![Full Screenshot](images/assignment5pic1.PNG)
![Plot](images/plot1.PNG)
![Plot](images/plot2.PNG)

