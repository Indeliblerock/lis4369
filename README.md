> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 Extensible Enterprise Systems

## Jonathan Poteet

### LIS 4369 Requirements:

*Course Work Links*

1. [A1 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/a1/README.md/ "Assignment 1")
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create *a1_tip_calculator* application
    * Provide screenshots of installations
    * Create Bitbucket Repo
    * Complete Bitbucket Tutorial
    * Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/a2/README.md/ "Assignment 2")
    * Backward Engineer the screenshots
    * Create Payroll Calculator using functions
    * Create it using modules *functions* and *main*
3. [A3 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/a3/README.md/ "Assignment 3")
    * Backwards engineer the screenshots
    * Create Painting Estimator using two modules
    * Use five functions: main, get_requirements, estimate_painting_cost, print_painting_estimate, print_painting_percentage
    * Use estimate_painting_cost to call print_painting_estimate and print_painting_percentage
    * Use a iteration structure to ask to continue the program        
4. [A4 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/a4/README.md/ "Assignment 4")
    * Create program that pulls Titanic passenger data into a program
    * Backwards engineer the screenshots
    * Create demo.py
5. [A5 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/a5/README.md/ "Assignment 5")
    * Use R and RStudio to perform data analysis on Titanic passenger data
    * Go through the R Tutorial
    * Display several plots using R 
6. [P1 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/p1/README.md/ "Project 1")
    * Download files using pip package installer
    * Create demo.py that imports pandas, pandas_datareader, and matplotlib
    * Use commands to manipulate the viewing of the data
    * Use three functions: main, get_requirements, and data_analysis_1
    * Display a graph using the data
7. [P2 README.md](https://bitbucket.org/Indeliblerock/lis4369/src/master/p2/README.md/ "Project 2")
    * Used R to perform data analysis on mtcars
    * Used R to display plots of the data
    * Backwards engineer the txt file