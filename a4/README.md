# LIS 4369 Extensible Enterprise Solutions

## Jonathan Poteet

### Assignment 4 Requirements:

*3 Things*

1. Code and run demo.py
2. Backwards engineer the screenshots
3. Why is the graph line split?
 - Age is not recorded for every individual in the data, so the split showcases the missing data.

#### README.md file should include the following items:

* Assignment requirements
* README file requirements
* Assignment screenshot in Visual Studio
* Assignment Screenshots in IDLE


#### Assignment Screenshots:

*Screenshot of program running in Visual Studio*:

![Visual Studio Screenshot](images/VisualStudio.PNG)

*Screenshot of program running in IDLE*:

![IDLE Screenshot](images/IDLE1.PNG)

*Screenshot of code*:

![Code Screenshot 1](images/code1.PNG)
![Code Screenshot 2](images/code2.PNG)
![Code Screenshot 3](images/code3.PNG)
![Code Screenshot 4](images/code4.PNG)
![Code Screenshot 5](images/code5.PNG)
![Code Screenshot 6](images/code6.PNG)

*Screenshot of output*
![Output 1](images/running8.PNG)
![Output 2](images/running7.PNG)
![Output 3](images/running6.PNG)
![Output 4](images/running5.PNG)
![Output 5](images/running4.PNG)
![Output 6](images/running3.PNG)
![Output 7](images/running2.PNG)
![Output 8](images/running1.PNG)
