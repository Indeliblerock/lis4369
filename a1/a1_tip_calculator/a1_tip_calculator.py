print("\nTip Calculator\n"
+ "\nProgram Requirements: \n"
+ '1. Must use float data type for user input (except, "party number").\n'
+ "2. Must round calculations to two decimal places. \n"
+ "3. Must format currency with dollar sign, and two decimal places. \n"
+ "\nUser Input\n") 
mealCost = float(input("Cost of meal: "))
percentTax = float(input("Tax Percent: "))
percentTip = float(input("Tip Percent: "))
partyNumber = int(input("Party Number: "))
totalTax = (mealCost * percentTax/100)
amountDue = mealCost + totalTax
gratuity = (amountDue * percentTip/100)
total = amountDue + gratuity
split = total / partyNumber

print("\nProgram Output: \n")
print("Subtotal: \t$" + "{:.2f}".format(mealCost))
print("Tax: \t\t$" +"{:.2f}".format(totalTax))
print("Amount Due: \t$" + "{:.2f}".format(amountDue))
print("Gratuity: \t$" + "{:.2f}".format(gratuity))
print("Total: \t\t$" + "{:.2f}".format(total))
print("Split(" + str(partyNumber) + "):  \t$" + "{:.2f}".format(split) + "\n")


