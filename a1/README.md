
# LIS 4369 Extensible Enterprise Systems

## Jonathan Poteet

### Assignment 1 Requirements:

*Four parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket Repo Links: 
    * a) This assignment and
    * b) The completed tutorial (bitbucketstationlocations)


#### README.md file should include the following items:

* Screenshot of a1_tip_calculator running
* Git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new repository
2. git status - Lists changes to the files in the staging area
3. git add - Adds files to the staging area
4. git commit - Commits files or changes to files to files in the local repository
5. git push - Sends changes from the local repository to the remote repository
6. git pull - Takes changed files from the remote repository, and brings them to the local repository
7. git clone - Creates a copy of the working repository

#### Assignment Screenshots:

*Screenshot of Visual Studio a1_tip_calculator*:

![Visual Studio](/a1/a1_visualstudio.PNG)

*Screenshot of running IDLE a1_tip_calculator*:

![IDLE screenshot](/a1/a1_idle.PNG)

#### Installation Screenshots:
*Python Installation* 

![Python Screenshot](/a1/pythoninstallationscreenshot.PNG)

*R Installation*

![R Screenshot](/a1/rinstallationscreenshot.PNG)

*R Studio installation*

![R Studio Screenshot](/a1/rstudioscreenshot.PNG)

*Visual Studio Installation*

![Visual Studio Screenshot](/a1/visualstudioscreenshot.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Link](https://bitbucket.org/Indeliblerock/bitbucketstationlocations/ "Bitbucket Station Locations")
