# LIS4369 Mobile Web Application Development

## Jonathan Poteet

### Assignment 3 Requirements:

*6 Things*

1. Backwards engineer the screenshots
2. Organize the program into two modules functions.py and main.py
3. Use a estimate_painting_cost function to calculate the home painting cost
4. Use a estimate_painting_cost function to call two other functions to print the result of the calculations
5. Use a Iteration structure to ask to continue the program
6. Print a link to my personal website or LinkedIn

#### README.md file should include the following items:

* Assignment Requirements
* Screenshots of the program in IDLE and Visual Studio

#### Assignment Screenshots:

*Screenshot of running Painting Estimator in IDLE*:

![IDLE painting Calculator Screenshot](/a3/images/PaintingEstimatorIDLE.PNG)

*Screenshot of running Painting Estimator in Visual Studio*:

![Visual Studio Calculator Screenshot](/a3/images/PaintingEstimator.PNG)

*Screenshots of the code in functions.py get_requirements*:

![Visual Studio Calculator get_requirements Screenshot](/a3/images/functionpy1.PNG)

*Screenshot of the code in functions.py get_painting_estimate*:

![Visual Studio Calculator get_painting_estimate Screenshot](/a3/images/functionpy2.PNG)

*Screenshot of main.py main()*:

![Screenshot of main.py](/a3/images/mainpy.PNG)
