import functions as f

def main():
    go = "y"
    while go == "y":
        f.get_requirements()
        f.estimate_painting_cost()
        go = input("\n\nWould you like to calculate another home price? (Enter y to continue or press any key to exit): ")
    print ("\nThank you for using our painting estimator!"
    + "\nPlease see our website: https://www.linkedin.com/in/jonathan-poteet/")
if __name__ == "__main__":
    main()