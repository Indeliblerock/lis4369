def get_requirements():
    print("\nPainting Estimator\n")
    print("Program Requirements:\n"
    + "1. Calculate Home Interior Paint Cost (w/o primer)\n"
    + "2. Must use float data types\n"
    + "3. Must use SQFT_PER_GALLON constant (350)\n"
    + '4. Must use iteration structure (aka "loop")\n'
    + "5. Format right-align numbers, round to two decimal places.\n"
    + "\ta. main(): Calls two other functions: get_requirements() and estimate_painting_cost()\n"
    + "\tb. get_requirements(): Displays the program requirements.\n"
    + "\tc. estimate_painting_cost(): calculate interior home painting, and calls print function.\n"
    + "\td. print_painting_estimate(): displays painting costs.\n"
    + "\te. print_painting_percentage(): displays painting costs percentage.\n")

def estimate_painting_cost():
    SQFT_PER_GALLON = 350
    print("\nInput: ")
    total_sq_ft = float(input("Enter total interior sq ft: ")) 
    price_per_gallon = float(input("Enter price per gallon paint: ")) 
    hourly_rate = float(input("Enter hourly painting rate per sq ft: ")) 
    number_gallons = total_sq_ft / SQFT_PER_GALLON 
    paint_amount = number_gallons * price_per_gallon 
    labor_amount = total_sq_ft * hourly_rate
    total_amount = paint_amount + labor_amount
    percentage_paint = (paint_amount / total_amount) * 100
    percentage_labor = (labor_amount / total_amount) * 100
    percentage_total = percentage_paint + percentage_labor
    print("\nOutput:")
    
    def print_painting_estimate():
        
        print("{0:25} {1:>6}".format("Item", "Amount") 
        +"\n" + "{0:25} {1:>.2f}".format("Total Sq Ft:", total_sq_ft ) 
        +"\n" + "{0:25} {1:>.2f}".format("Sq Ft Per Gallon:", SQFT_PER_GALLON) 
        +"\n" + "{0:25} {1:>.2f}".format("Number of Gallons:", number_gallons) 
        +"\n" + "{0:21} {1:>3} {2:>.2f}".format("Paint Per Gallon", "$", price_per_gallon) 
        + "\n" + "{0:21} {1:>3} {2:>.2f}".format("Labor Per Sq Ft", "$", hourly_rate))
    
    def print_painting_percentage():

        
        print("{0:17} {1:15} {2:10}".format("Cost", "Amount", "Percentage") 
        + "\n" + "{0:17} {1:1} {2:2<.2f} {3:12.2f}{4:2}".format("Paint:", "$", paint_amount, percentage_paint, "%")
        + "\n" + "{0:17} {1:1} {2:2<.2f} {3:12.2f}{4:2}".format("Labor:", "$", labor_amount, percentage_labor, "%")
        + "\n" + "{0:17} {1:1} {2:2<.2f} {3:12.2f}{4:2}".format("Total:", "$", total_amount, percentage_total, "%"))
    
    print_painting_estimate()
    print("")
    print_painting_percentage()   
    