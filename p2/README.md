
# LIS4369 Extensible Enterprise Solutions

## Jonathan Poteet

### Project 2 Requirements:

*4 things*

1. Backward-engineer the lis4369_p2_requirements.txt file 
2. Be sure to include at least two plots in your README.md file
3. Create plots using R
4. Use R to perform data analysis on mtcars

#### README.md file should include the following items:

* Assignment requirements for project 2
* Screenshots of output of code
* Screenshots of plots


#### Assignment Screenshots:

*Plots*:

![Plot 1](images/plot1.PNG)

![Plot 2](images/plot2.PNG)

*Output*:

![Output 1](images/output1.PNG)

![Output 2](images/output2.PNG)

![Output 3](images/output3.PNG)

![Output 4](images/output4.PNG)

![Output 5](images/output5.PNG)

![Output 6](images/output6.PNG)

![Output 7](images/output7.PNG)

![Output 8](images/output8.PNG)

![Output 9](images/output9.PNG)

