def get_requirements():
    print("\nIT/ICT Student Percentage\n")
    print("Program Requirements\n"
    + "1. Find number of IT/ICT students in class.\n"
    + "2. Calculate IT/ICT Student Percentage.\n"
    + "3. Must use float data type (to facilitate right-alignment)\n"
    + "4. Format, right-align numbers, and round to two decimal places.\n\n"
    + "Input: ")

def calculate_student_percentage():

    itStudents = float(input("Enter number of IT students: "))
    ictStudents = float(input("Enter number of ICT students: "))
    totalStudents = itStudents + ictStudents
    itStudentPercentage = itStudents/totalStudents
    ictStudentPercentage = ictStudents/totalStudents
    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("Total Students:", totalStudents) 
    + "\n" + "{0:17} {1:>5.2%}".format("IT Students:", itStudentPercentage)
    + "\n" + "{0:17} {1:>5.2%}".format("ICT Students: ", ictStudentPercentage))