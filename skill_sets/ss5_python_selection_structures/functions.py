def get_requirements():
    print("\nPython Selection Structures\n")

    print("Program Requirements\n"
    + "1. Use Python selection structure.\n"
    + "2. Prompt user for two numbers, and a suitable operator.\n"
    + "3. Test for correct numeric operator.\n"
    + "4. Replicate display below.\n\n"
    + "Python Calculator")

def calculate_calculator():
    import math
    num1 = float(input("Enter num1: ")) 
    num2 = float(input("Enter num2: "))
    print("Suitable operators: +, -, *, /, // (integer division), % (modulo operator), ** (power) or math.pow()")
    operator = input(str("Enter operator: "))
    
    if operator == "+":
        total = num1 + num2
        print("{0:.1f}".format(total))
    elif operator == "-":
        total = num1 - num2
        print("{0:.1f}".format(total))
    elif operator == "*":
        total = num1 * num2
        print("{0:.1f}".format(total))
    elif operator == "/":
        total = num1 / num2
        print("{0:.1f}".format(total))
    elif operator == "//":
        total = num1 // num2
        print("{0:.1f}".format(total))
    elif operator == "%":
        total = num1 % num2
        print("{0:.1f}".format(total))
    elif operator == "**":
        total = num1 ** num2
        print("{0:.1f}".format(total))
    elif operator == "math.pow()":
        total = math.pow(num1, num2)
        print("{0:.1f}".format(total))
    else:
        print("Incorrect operator.")