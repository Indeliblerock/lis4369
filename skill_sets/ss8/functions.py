def get_requirements():
    print("\nPython Tuples\n")

    print("Program Requirements\n"
    + "1. Tuples (Python data Structure): immutable, ordered sequence of elements/\n"
    + "2. Tuples are immutable/unchangeable--that is, cannot insert, update, delete\n"
    + "Note: can reassign or delete a entire tuple--but not the individual items or slices. \n"
    + '3.Create tuple using parentheses (tuple): my_tuple = ("cherries", "apples", "bananas", "oranges") \n'
    + '4. Create tuple (packing), that is, *without* using paranthesis (aka tuple "packing"): my_tuple2 = 1, 2, "three", "four" \n'
    + "5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables: fruit1, fruit2, fruit3, fruit4 = my_tuple1 \n"
    + "6. Create a program that mirrors the IPO (input, process, output) format.\n\n"
    + "Note: user enters number of requested list elements, dynamically entered below (that is elements can change each run.)")

def start_tuple():
    print("\nInput:\n")
    my_tuple1 = ("cherries", "apples", "bananas", "oranges")
    my_tuple2 = 1, 2, "three", "four"
    fruit1, fruit2, fruit3, fruit4 = my_tuple1
    print("Hard coded -- no user input\n")

    print ("\nOutput:\n")
    print("\nPrint my_tuple1:")
    print(my_tuple1)
    print("\nPrint my_tuple2:")
    print(my_tuple2)
    print("\nPrint my_tuple1 unpacking:")
    print(fruit1, fruit2, fruit3, fruit4)
    print("\nPrint third element in my_tuple2:")
    print(my_tuple2[2])
    print("\nPrint *slice* of my_tuple1:")
    print(my_tuple1[1:3])
    print("\nReassign my_tuple2 using parenthesis.")
    print("Print my_tuple2:")
    my_tuple2 = (1, 2, 3, 4)
    print(my_tuple2)
    print('\nReassign my_tuple2 using "packing method" (no parenthesis)')
    print("Print my_tuple2:")
    my_tuple2 = 5, 6, 7, 8
    print(my_tuple2)
    print("\nPrint number of elements in my_tuple1:")
    print(len(my_tuple1))
    print("\nPrint type of my_tuple1: <class 'tuple'> ")
    print(type(my_tuple1))
    print("\nDelete my_tuple2:")
    del my_tuple2
    #print(my_tuple2)