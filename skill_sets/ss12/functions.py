def get_requirements():
    print("\nPython Temperature conversion\n")

    print("Program Requirements\n"
    + "1. Program converts user-entered temperatures into Fahrenheit or Celsius scales.\n"
    + "2. Program continues to prompt for user entry until no longer requested.\n"
    + "3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.\n"
    + "4. Note: Program does not validate numeric data (optional requirement)\n")
    

def start_temp():
    print("\n\nInput:")
    type_value = ''
    go = ''
    go = input("Do you want to convert a temperature (y or n)? ")
    go = go.lower()
    value_calc = 0.0
    print("\nOutput:")
    while (go[0] == 'y'):
        
        type_value = input("Fahrenheit to celsius? Type 'f' or Celsius to Fahrenheit? Type 'c': ")
        type_value = type_value.lower()
        if (type_value[0] == "c"):
            value_calc = float(input("Enter temperature in Celsius: "))
            
            value_calc = (value_calc * 9/5) + 32
            print("Temperature in Celsius: " + str(value_calc))
            go = input("\nDo you want to convert a temperature (y or n)? ").lower()
        elif (type_value[0] == "f"):
            value_calc = float(input("Enter temperature in Fahrenheit: "))
            value_calc = ((value_calc -32)*5)/9
            print("Temperature in Fahrenheit: " + str(value_calc))   
            go = input("\nDo you want to convert a temperature (y or n)? ").lower()
        else:
            print("Incorrect entry, please try again?\n")
            go = input("\nDo you want to convert a temperature (y or n)? ").lower()
        
    print("Thank you for using our temperature conversion program!")