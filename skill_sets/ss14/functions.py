def get_requirements():
    print("\nPython Calculator with Error Handling\n")

    print("Program Requirements\n"
    + "1. Program calculates two numbers, and rounds to two decimal places.\n"
    + "2. Prompt user for two numbers, and a suitable operator.\n"
    + "3. Use Python error handling to validate data.\n"
    + "4. Test for correct arithmetic operator.\n"
    + "5. Division by zero not permitted.\n"
    + "6. Note: Program loops until input entered - numbers and arithmetic operator.\n"
    + "7. Replicate display below.\n")
    

def start_calc():
    import math
    i = 0
    list_num = []
    calculation = 0.0
    while (i < 2):
        num = input("Enter num"+ str(i + 1) +": ")
        if (num.isdigit()):
            num = int(num)
            list_num.append(num)
            i = i + 1
        else:
            print("Not valid number!")
    
    num1 = list_num[0]
    num2 = list_num[1]

    #prompt for operator.
    print("\nSuitable operators: +, -, *, /, //(integer division), % (modulo operator), **(power)")
    
    go = 1    
    while (go == 1):
        operator = input("\nEnter operator: ")
        if (operator == "+"):
            calculation = num1 + num2
            go = 0
            print("{0:.2f}".format(calculation))

        elif (operator == "-"):
            calculation = num1 - num2
            go = 0
            print("{0:.2f}".format(calculation))

        elif (operator == "*"):
            calculation = num1 * num2
            go = 0
            print("{0:.2f}".format(calculation))

        elif (operator == "/"):
            if (num2 != 0):
                    #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                    calculation = num1 / num2
                    go = 0
                    print("{0:.2f}".format(calculation))
            else:
                    while (num2 == 0):
                        print("Cannot divide by zero!")
                        num2 = str(num2)
                        
                        if (num2.isdigit()):
                            go1 = True
                            
                            while (go1 == True):
                                num2 = input("\nPlease enter Num2: ")
                                if (num2.isdigit()):
                                    num2 = int(num2)
                                    go1 = False 
                                else:
                                    print("Not valid number!")#something with this
                        else:
                            print("Not valid number!")
                    if (num2 != 0):
                        #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                        calculation = num1 / num2
                        go = 0
                        print("{0:.2f}".format(calculation))
                    
        
        elif (operator == "//"):
            if (num2 != 0):
                    #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                    calculation = num1 // num2
                    go = 0
                    print("{0:.2f}".format(calculation))
            else:
                    while (num2 == 0):
                        print("Cannot divide by zero!")
                        num2 = str(num2)
                        
                        if (num2.isdigit()):
                            go1 = True
                            
                            while (go1 == True):
                                num2 = input("\nPlease enter Num2: ")
                                if (num2.isdigit()):
                                    num2 = int(num2)
                                    go1 = False 
                                else:
                                    print("Not valid number!")#something with this
                        else:
                            print("Not valid number!")
                    if (num2 != 0):
                        #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                        calculation = num1 // num2
                        go = 0
                        print("{0:.2f}".format(calculation))
        
        
        
        elif (operator == "%"):
            if (num2 != 0):
                    #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                    calculation = num1 % num2
                    go = 0
                    print("{0:.2f}".format(calculation))
            else:
                    while (num2 == 0):
                        print("Cannot divide by zero!")
                        num2 = str(num2)
                        
                        if (num2.isdigit()):
                            go1 = True
                            
                            while (go1 == True):
                                num2 = input("\nPlease enter Num2: ")
                                if (num2.isdigit()):
                                    num2 = int(num2)
                                    go1 = False 
                                else:
                                    print("Not valid number!")#something with this
                        else:
                            print("Not valid number!")
                
                    if (num2 != 0):
                        #FIX THIS THING DIVIDE BY ZERO NOT ALLOWED 
                        calculation = num1 % num2
                        go = 0
                        print("{0:.2f}".format(calculation))
        
        
        elif (operator == "**"):
            calculation = num1 ** num2
            calculation = pow(num1, num2)
            go = 0
            print("{0:.2f}".format(calculation))
        else:
            print("Incorrect operator!")
    
    #print("{0:.2f}".format(calculation))
    
    print("\nThank you for using our Math Calculator!")
        
        
