def get_requirements():
    print("\nPython Looping Structures\n")

    print("Program Requirements\n"
    + "1. Print while loop.\n"
    + "2. print for loops using range() function, and implicit and explicit lists.\n"
    + "3. Use break and continue statements.\n"
    + "4. Replicate display below.\n\n"
    + "Note: In python, for loop is used for iterating over a sequence (i.e., List, Tuple, dictionary, set, or string)")

def start_loop():
    
    print("\n1. While Loop")
    i = 1
    while (i <= 3):
        print (i)
        i = i + 1
        
    print("\n2. for loop: using range() function with 1 argument:")
    for i in range(4):
        print (i)
    
    print("\n3. for loop: using range() function with 2 arguments:")
    for i in range(1, 4):
        print (i)

    print("\n4. for loop: using range() function with 3 arguments (interval 2):")
    for i in range(1, 4, 2):
        print (i)

    print("\n5. for loop: using range() function with 3 arguments (negative interval):")
    for i in range(3, 0, -2):
        print (i)

    print("\n6. for loop: using (implicit) list:")
    for i in [1, 2, 3]:
        print (i)
    
    print("\n7. for loop: iterating through (explicit) string list:")
    list1 = ["Michigan", "Alabama", "Florida"]
    for i in list1:
        print (i)
    
    print("\n8. for loop: using break statement (stops loop):")
    for i in list1:
        if (i == "Florida"):
            break
        else:
            print (i)

    print("\n9. for loop: using continue statement (stops loop and continues with next):")
    for i in list1:
        if (i == "Alabama"):
            continue
        else:
            print (i)
        
    
    print("\n10. print list length:")
    print(len(list1))