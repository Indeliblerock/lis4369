def get_requirements():
    print("\nPython Dictionaries\n")

    print("Program Requirements\n"
    + "1. Get user beginning and ending integer values, and store in two variables.\n"
    + "2. Display 10 pseudo-random numbers between and including, above values."
    + "3. Must use integer data types.\n" 
    + "4. Example 1: using range() and randint() functions. \n"
    + "5. Example 2: Using a list with range() and shuffle() functions.")
    

def start_rand():
    import random
    from random import shuffle
    print("\n\nInput:")
    beg_value = int(input("Beginning Value: "))
    end_value = int(input("Ending Value: "))
   
    print("Output:")
    print("Example 1: using range() and randint() functions.")
    for i in range(10):
        print(random.randint(beg_value, end_value), end=" ")

    print("\nExample 2: Using a list with range() and shuffle() functions.")
    r = list(range(beg_value, end_value + 1))
    random.shuffle(r)
    for i in r:
        print(i, end= " ")
    print()