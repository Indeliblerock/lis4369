def get_requirements():
    print("\nPython Looping Structures\n")

    print("Program Requirements\n"
    + "1. Lists (python data structure): mutable, ordered sequence of elements.\n"
    + "2. Lists are mutable/changeable -- that is, can insert, update, delete\n"
    + '3. Create list using square brackets [list]: myList = ["cherries", "apples", "bananas", "oranges"].\n'
    + "4. Create a program that mirrors the IPO (input, process, output) format.\n\n"
    + "Note: user enters number of requested list elements, dynamically entered below (that is elements can change each run.)")

def start_list():
    print("\nInput:\n")
    #first selection
    num = int(input("Enter number of list elements: "))
    i=0
    my_list = []
    #establishing list (could use for i in range(num):)
    while(i != num):
        value = input("Please enter a list element: ")
        my_list.append(value)
        i = i + 1
    #printing general list    
    print("\nOutput:\n"
    + "Print my_list:")
    print(my_list)
    print("\nInserts value in list:")

    #take values for insertion
    element_value = input("Please enter list element: ")
    list_value = int(input("Please enter list *index* position. (note: must convert to int): "))
    #inserts value in list
    my_list.insert(list_value, element_value)
    print(my_list)

    #Counts number of items in list
    print("\nCounts number of items in list:")
    print(len(my_list))

    #sort elements in list alphabetically
    print("\nSort elements in list alphabetically:")
    my_list.sort()
    print(my_list)

    #Reverses list
    print("\nReverses list:")
    my_list.reverse()
    print(my_list)

    #remove last list element (pop)
    print("\nRemove last list element:")
    my_list.pop()
    print(my_list)

    #delete second element from list by index
    print("\nDelete second element from list by index:")
    my_list.pop(1)
    print(my_list)

    #delete element from list by value ("cherries")
    print('\nDelete element from list by value ("cherries"):')
    my_list.remove("cherries")
    print(my_list)

    #Delete all elements from list: (could use "del my_list[:]" or "my_list.clear()")
    print("\nDelete all elements from list:")
    while (len(my_list) != 0):
        my_list.pop()
    print(my_list)