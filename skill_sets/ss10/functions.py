def get_requirements():
    print("\nPython Dictionaries\n")

    print("Program Requirements\n"
    + "1. Dictionaries (python data structures) unordered key:value pairs\n"
    + "2. Dictionary: an associative array (also known as hashes)\n"
    + "3. Any key in a dictionary is associated (or mapped) to a value (i.e., any python data type).\n"
    + "4. Keys: must be of immutable type (string, number, or tuple with immutable elements) and must be unique.\n"  
    + "5. Values can be of any data type and can repeat.\n"
    + "6. Create a program that mirrors the following IPO (input, process, output) format.\n"
    + "\tCreate empty dictionary, using curly braces {}: my_dictionary = {}\n"
    +"\tUse the following keys: fname, lname, degree, major, gpa \n"
    + "Note: dictionaries have key-value pairs instead of single values; This differentiates a dictionary from a set.")
    

def start_dictionary():
    print("\n\nInput:")
    fname = input("First name: ")
    lname = input("Last name: ")
    degree = input("Degree: ")
    major = input("Major (IT or ICT): ")
    gpa = round(float(input("GPA: ")), 2)
    
    my_dictionary = {'fname':fname, 'lname':lname, 'degree':degree, 'major':major, 'gpa':gpa}

    print("\n\nOutput:")
    print("Print my_dictionary:")
    print(my_dictionary)

    print("\nReturn view of dictionary's (key, value) pair, built-in function:")
    print(my_dictionary.items())

    
    print("\nReturn view object of all keys, built-in function:")
    print(my_dictionary.keys())
    
    print("\nReturn view object of all values in dictionary, built-in function:")
    print(my_dictionary.values())
    
    print("\nPrint only first and last names, using keys:")
    print(my_dictionary['fname'] + " " + my_dictionary['lname'])
    
    print("\nPrint only first and last names, using get() function:")
    print(my_dictionary.get('fname') + " " + my_dictionary.get('lname'))
    
    print("\nCount number of items (key:value pairs) in dictionary:")
    print(len(my_dictionary))
    
    print("\nRemove last dictionary item (popitem):")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary, using key:")
    del my_dictionary['major']
    print(my_dictionary)
    
    print("\nReturn object type:")
    print(type(my_dictionary))
    
    print("\nDelete all items from list:")
    my_dictionary.clear()
    print(my_dictionary)