def get_requirements():
    print("\nPython Temperature conversion\n")

    print("Program Requirements\n"
    + "1. Program calculates sphere volume in liquid U.S gallons from user-entered diameter value in inches, and rounds to two decimal places.\n"
    + "2. Must use Python's *built-in* Pi and pow() capabilities.\n"
    + "3. Program checks for non-integers and non-numeric values.\n"
    + "4. Program continues to prompt for user entry until no longer requested, prompt accepts upper of lower case letters.\n")
    

def start_sphere():
    import math
    print("\n\nInput:")
    go = ''
    calculation = 0.0
    go = input("Do you want to calculate a sphere volume (y or n)? ").lower()
    print("\nOutput: ")
    while (go == 'y'):

        diameter = input("\nPlease enter diameter in inches: ")
        
        if (diameter.isdigit()):
            diameter = int(diameter)
            radius = diameter / 2.0
            calculation = ((4.0/3.0) * math.pi * pow(radius, 3))
            calculation = calculation/231    
            print("Sphere volume: ", '{0:.2f}'.format(calculation), "liquid U.S. gallons.")
            go = input("\nDo you want to calculate a sphere volume (y or n)? ").lower()
        else:
            print("Not valid integer!")    
        
    print("\nThank you for using our Sphere Volume Calculator!")
        
        
