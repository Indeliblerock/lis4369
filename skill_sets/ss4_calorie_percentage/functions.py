def get_requirements():
    print("\nCalorie Percentage\n")

    print("Program Requirements\n"
    + "1. Find calories per grams of fat, carbs, and protein.\n"
    + "2. Calculate percentages.\n"
    + "3. Must use float data types.\n"
    + "4. Format, right-align numbers, and round to two decimal places.\n\n"
    + "Input: ")

def calculate_calorie_percentage():

    fatGrams = float(input("Enter total fat grams: ")) * 9
    carbGrams = float(input("Enter total carb grams: ")) * 4
    proteinGrams = float(input("Enter total protein grams: ")) * 4
    total = fatGrams + carbGrams + proteinGrams
    

    print("\nOutput:")
    print("{0:8} {1:>10} {2:>13}".format("Type", "Calories", "Percentage")
    + "\n" + "{0:8} {1:>10,.2f} {2:13,.2%}".format("Fat", fatGrams, (fatGrams / total)) 
    + "\n" + "{0:8} {1:>10,.2f} {2:13,.2%}".format("Carbs", carbGrams, (carbGrams / total))
    + "\n" + "{0:8} {1:>10,.2f} {2:13,.2%}".format("Protein", proteinGrams, (proteinGrams / total))
    + "\n")