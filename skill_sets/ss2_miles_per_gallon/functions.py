def get_requirements():
    print("\nMiles Per Gallon\n")
    print("Program Requirements\n"
    + "1. Convert MPG.\n"
    + "2. Must use float data type for user input and calculation.\n"
    + "3. Format and round conversion to two decimal places.\n\n"
    + "Input: ")

def calculate_miles_per_gallon():
    milesDriven = 0.0
    gallonsUsed = 0.0
    milesPerGallon = 0.0
    milesDriven = float(input("Enter miles driven: "))
    gallonsUsed = float(input("Enter gallons of fuel used: "))
    milesPerGallon = milesDriven / gallonsUsed
    print("\nOutput:")
    print("{:.2f}".format(milesDriven) + " miles driven and " + "{:.2f}".format(gallonsUsed) 
    + " gallons used = " + "{:.2f}".format(milesPerGallon) + " mpg\n")